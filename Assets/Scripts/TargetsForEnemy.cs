using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TargetsForEnemy : MonoBehaviour
{
    private List<Transform> targets = new List<Transform>();

    public static TargetsForEnemy Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Debug.Log("TargetsForEnemy Awake");
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddTarget(Transform targetTransform)
    {
        targets.Clear();
        targets.Add(targetTransform);
        Debug.Log("AddTarget");
    }

    public Transform GetTarget()
    {
        Debug.Log("GetTarget");
        Debug.Log(targets[0].name);
        return targets[0];
    }
}
